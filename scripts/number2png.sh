DIR="$(mktemp -d)"
pdflatex -output-directory "$DIR" >/dev/null <<EOF
\documentclass[preview,border=2pt]{standalone}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{lmodern}
\usepackage{microtype}
\usepackage{amsmath}
\usepackage{amssymb,latexsym}
\renewcommand*\familydefault{\sfdefault}
\begin{document}
$1
\end{document}
EOF
DPI=$2
[ -z "$DPI" ] && DPI=600 
pdftoppm -png -f 1 -l 1 -aa no -r "$DPI" "$DIR/texput.pdf" > "$DIR/texput.png"  
convert "$DIR/texput.png" -transparent white "$DIR/out.png"
cat "$DIR/out.png"
rm -rf "$DIR"
unset DIR
