#!/bin/bash

cd "$(dirname "$0")/.."

DIRNAME=labels

[ ! -d $DIRNAME ] && mkdir $DIRNAME

for i in {1..9999}; do
    printf "scripts/number2png.sh %s > $DIRNAME/%04d.png\n" $i $i
done | parallel
