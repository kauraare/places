# Places
Simple webserver to visualize places on Google Maps and to find places which are the farthest away from

### Usage
`data/data.csv` contains the database of names of the places, latitudes and longitudes.
Run `scripts/genlabels.sh` to generate labels to display on the map.

### Example
A working example of such webserver is hosted on https://places.kaur.pw/
