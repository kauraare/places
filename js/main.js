var earthsradius = 6378;
var southPoleTriangle = [{lat: -89.99, lng: 0}, {lat: -89.99, lng: -120}, {lat: -89.99, lng: 120}]; // triangle around south pole
var anyPointInfowindow = null;
var ExtrmInfowindow = null;
var places = null;
var textfile = null;
var mapsstyle = null;
var polygonarray = [];
var markerarray = [];

$.ajax({
  'async': false,
  'global': false,
  'url': "style/mapsstyle.json",
  'dataType': "json",
  'success': function (data) {
    mapsstyle = data;
  }
});

$.ajax({
    'async': false,
    'global': false,
    'url': 'data/data.csv',
    'dataType': "text",
    'success': function (data) {
        textfile = data;
    }
});
var places = $.csv.toObjects(textfile)
for (city=0; city < places.length; city++) {
    places[city].lng=Number(places[city].lng);
    places[city].lat=Number(places[city].lat)
}

function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 3,
    center: {lat: 20, lng: 10},
    mapTypeId: 'roadmap',
    styles: mapsstyle
  });

  map.addListener('click', function(e) {
    placeMarker(e.latLng, map);
  });

//  Polygon.addListener('click', function(e) {
//    placeMarker(e.latLng, map);
//  });

  google.maps.event.addDomListener(button_clear, 'click', function() {
    clearMap(map)
  });
  google.maps.event.addDomListener(button_radius, 'click', function() {
    drawAllCircles(map)
  });
  google.maps.event.addDomListener(button_extrema, 'click', function() {
    findExtrema(map)
  });

  drawAllCircles(map)
}


function drawCircle(point, radius)  {
  var points = 32; // number of points in circle

  var b = radius / earthsradius ; // Radius of circle in rad
  var c = (90 - point.lat) / 180 * Math.PI  ; // Distance of center from pole in rad

  var extp = new Array();
  for (var i=0; i<points+1; i++) {
    var alpha = 2 * Math.PI * (i / points); // angle
    var a = Math.acos ( Math.cos(b) * Math.cos(c) + Math.sin(b) * Math.sin(c) * Math.cos(alpha)) ; // distance of new point to pole
    var sinbeta = Math.sin(alpha) / Math.sin(a)* Math.sin(b);
    var cosbeta = Math.min(1, Math.max(-1, (Math.cos(b) - Math.cos(a)*Math.cos(c)) / (Math.sin(a) * Math.sin(c)))); // cos of angle between medians
    var beta = Math.atan2(sinbeta,cosbeta)

    var ex = 90 - 180 / Math.PI * a ; // new point latitude
    var ey = point.lng + 180 / Math.PI * beta ; // new point longitude
    extp.push(new google.maps.LatLng(ex, ey));
  }
  return extp;
}

function drawAllCircles(map) {
  var Ncircles=document.getElementById('Ncircles').value;
  var Rmin=document.getElementById('min_radius').value;
  var Rmax=document.getElementById('max_radius').value;
  var step=Math.pow((Rmax/Rmin),1/(Ncircles-1))
  for (var i=0; i<Ncircles; i++) {
    var radius = Rmin*Math.pow(step,i);
    var opacity = 0.4*Math.pow(0.6,i);
    var paths = new Array();
    for (city=0; city < places.length; city++) {
      paths.push(drawCircle({lat: places[city].lat, lng: places[city].lng}, radius))
      if ((90 + places[city].lat) - radius/earthsradius*180/Math.PI < 0) {paths.push(southPoleTriangle)}
    }

    var Polygon = new google.maps.Polygon({
      paths: paths,
      strokeColor: "#000000",
      strokeOpacity: 0.5,
      strokeWeight: 0,
      fillColor: "#FF0000",//color[i],
      fillOpacity: opacity
    });
    polygonarray.push(Polygon);
    Polygon.setMap(map);
  }
}

function clearMap(map) {
    for (var i = 0; i < polygonarray.length; i++ ) {
	polygonarray[i].setMap(null);
    }
    polygonarray.length = 0;

    for (var i = 0; i < markerarray.length; i++ ) {
	markerarray[i].setMap(null);
    }
    markerarray.length = 0;
    
}

function sortByDistance(v, places) {
  for (i=0; i<places.length; i++) {
    var a = (90-v[0])/180*Math.PI
    var b = (90-places[i].lat)/180*Math.PI
    var gamma = (places[i].lng-v[1])/180*Math.PI ;
    places[i].distance=earthsradius * Math.acos(Math.cos(gamma) * Math.sin(a) * Math.sin(b) + Math.cos(a) * Math.cos(b))
  }
  places.sort(function(a,b) {return a.distance-b.distance})
  return  places;
}

function placeMarker(latLng, map) {
  if (anyPointInfowindow) {
    anyPointInfowindow.close();
  }
  var v = [latLng.lat(), latLng.lng()]
  var sorted = sortByDistance(v,places)
  anyPointInfowindow = new google.maps.InfoWindow({
    content:  sorted[0].Place + ": "+ sorted[0].distance.toFixed(0) + " km" + " </br>" +
    sorted[1].Place + ": "+ sorted[1].distance.toFixed(0) + " km" + " </br>" +
    sorted[2].Place + ": "+ sorted[2].distance.toFixed(0) + " km" ,
    position: latLng,
    map: map
  });
  anyPointInfowindow.open(map);
}

function findExtrema(map) {
  for (var iter=0; iter<=document.getElementById('Npoints').value; iter++) {
    // objective that needs to be minimized
    fnc = function (v) {
      var sortedplaces = sortByDistance(v,places)
      return -sortedplaces[0].distance;
    };
    
    var xx = map.getBounds();
    x0=[
        xx.getSouthWest().lat() + Math.random() * xx.getNorthEast().lat(),
        xx.getSouthWest().lng() + Math.random() * xx.getNorthEast().lng()
    ]
    // x0 = [Math.random()*360-180, Math.random()*180-90]; // whole world
    // x0 = [Math.random()*40+35, Math.random()*55-15]; // europe
    
    var solution = optimjs.minimize_Powell(fnc, x0);
    var sorted = sortByDistance(solution.argument,places)

    if (sorted[2].distance-sorted[0].distance < 5 && sorted[0].distance > 100) {
        var marker = new google.maps.Marker({
            position: {lat: solution.argument[0], lng: solution.argument[1]},
            map: map,
            icon: {
                url: "labels/" + ("000" + sorted[0].distance.toFixed(0).toString()).slice (-4) + ".png",
                scaledSize: new google.maps.Size(50, 50)
            }
        })
	markerarray.push(marker);
	var string =          (-solution.fncvalue).toFixed(0) + ' km</br>' +
        sorted[0].Place + ": "+ sorted[0].distance.toFixed(0) + " km</br>" +
        sorted[1].Place + ": "+ sorted[1].distance.toFixed(0) + " km</br>" +
        sorted[2].Place + ": "+ sorted[2].distance.toFixed(0) + " km"
        bindInfoWindow(marker, map, string);
    }
  }
}

function bindInfoWindow(marker, map, string) {
  marker.addListener('click', function() {
    if (ExtrmInfowindow) {ExtrmInfowindow.close()}
    ExtrmInfowindow = new google.maps.InfoWindow();
    ExtrmInfowindow.setContent(string);
    ExtrmInfowindow.open(map, this);
  });
}

function findCenter(place1,place2,place3) {
  function sph2crt(place) {
    var theta = place.lat / 180 * Math.PI;
    var phi = place.lng / 180 * Math.PI;
    var x = Math.sin(theta) * Math.cos(phi);
    var y = Math.sin(theta) * Math.sin(phi);
    var z = Math.cos(theta);
    var v=new Vector(x,y,z)
    return v
  }

  function crt2sph(v) {
    rho = v.length();
    console.log(v.x + ',' + v.y + ',' + v.z)
    phi = Math.atan2(v.y, v.x);
    theta = Math.acos(v.z / rho);
    return {lat: theta*180/Math.PI, lng: phi*180/Math.PI , rho: rho }
  }

  var v1 = sph2crt(place1)
  var v2 = sph2crt(place2)
  var v3 = sph2crt(place3)

  var v21 = v2.subtract(v1)
  var v32 = v3.subtract(v2)
  var v13 = v1.subtract(v3)
  var denom =  2 * Math.pow(v21.cross(v32).length(),2)
  var alpha = - Math.pow(v32.length(),2) * v21.dot(v13) / denom
  var beta  = - Math.pow(v13.length(),2) * v32.dot(v21) / denom
  var gamma = - Math.pow(v21.length(),2) * v13.dot(v32) / denom

  var center = v1.multiply(alpha).add(v2.multiply(beta)).add(v3.multiply(gamma));
  var centersph = crt2sph(center)
  return centersph
}
